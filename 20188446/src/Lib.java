package xianmu1;
import java.io.*;
import java.util.*;
import java.util.regex.*;
public class Lib {
    private String inputFile;
    private String outputFile;
    private String content;
    private int charsNum;/*字符数*/
    private int wordsNum;/*单词总数*/
    private int linesNum;/*有效行数*/
    private Map<String, Integer> wordsMap;/*存放单词与次数*/
    public int getCharsNum() {
        return charsNum;
    }
    public int getLinesNum() {
        return linesNum;
    }
    public int getWordsNum() {
        return wordsNum;
    }
    public Map<String, Integer> getWordsMap() {
        return wordsMap;
    }
    public Lib(String inputFile, String outputFile) {
        this.inputFile = inputFile;
        this.outputFile = outputFile;
    }
    public void readFile() throws IOException {/*读取input.txt文件里的内容*/
        BufferedReader reader = null;
        StringBuilder builder = new StringBuilder();
        try{
            reader = new BufferedReader(new FileReader(inputFile));
            int num = 0;
            char ch;
            while ((num = reader.read()) != -1) {
                if (num != 13) {
                    ch = (char) num;
                    builder.append(ch);
                }
            }
        }
        catch (IOException e){
            e.printStackTrace();
        }
        finally {
            reader.close();
        }
        content = builder.toString();
    }
    public void writeFile() throws IOException {/*把代码运行后的结果写入到output.txt文件里*/
        countProcess();
        BufferedWriter writer = null;
        try{
            writer = new BufferedWriter(new FileWriter(outputFile));
            writer.write("characters: " + charsNum + "\n");
            writer.write("words: " + wordsNum + "\n");
            writer.write("lines: " + linesNum + "\n");
            List<Map.Entry<String, Integer>> topWords = sortWordsMap();
            int top = 0;
            for(Map.Entry<String, Integer> map : topWords){/*统计频率最高的10个单词*/
                if (top < 10){
                    writer.write(map.getKey() + ": " + map.getValue() + "\n");
                    top++;
                }
                else {
                    break;
                }
            }
            writer.flush();
        }
        catch (IOException e){
            e.printStackTrace();
        }
        finally {
            writer.close();
        }
    }
    public void countProcess(){
        countCharsNum();
        countWordsNum();
        countLinesNum();
    }
    public void countCharsNum(){ /*统计字符数*/
        charsNum = content.length();
    }
    public void countWordsNum(){/*统计单词总数*/
        wordsNum = 0;
        wordsMap = new HashMap<String,Integer>();
        String word;
        Integer num;
        String[] words = content.split("[^a-zA-Z0-9]+");
        for (int i = 0; i < words.length; i++){
            if (words[i].matches("[a-zA-Z]{4,}[a-zA-Z0-9]*")) {
                wordsNum++;
                word = words[i].toLowerCase();
                if (wordsMap.containsKey(word)){
                    num = wordsMap.get(word);
                    num++;
                    wordsMap.put(word, num);
                }
                else {
                    wordsMap.put(word, 1);
                }
            }
        }
    }
    public void countLinesNum(){/*统计有效行数*/
        linesNum = 0;
        Pattern linePattern = Pattern.compile("(^|\n)\\s*\\S+");
        Matcher matcher = linePattern.matcher(content);
        while (matcher.find()){
            linesNum++;
        }
    }
    public List<Map.Entry<String, Integer>> sortWordsMap(){/*统计频率最高的10个单词进行排序输出字典序靠前的单词*/
        List<Map.Entry<String, Integer>> wordsList = new ArrayList<Map.Entry<String, Integer>>(wordsMap.entrySet());
        Collections.sort(wordsList, new Comparator<Map.Entry<String, Integer>>() {
            public int compare(Map.Entry<String, Integer> word1,Map.Entry<String, Integer> word2) {
                if(word1.getValue().equals(word2.getValue())) {
                    return word1.getKey().compareTo(word2.getKey());
                }
                else {
                    return word2.getValue() - word1.getValue();
                }
            }
        });
        return wordsList;
    }
}